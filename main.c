﻿/*
 *Aluno : Gabriel Guimarães Cardoso
*https://gitlab.com/GabrielGCardoso/compilador
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//definições

//palavras reservadas
#define MAIN 1
#define IF 2
#define ELSE 3
#define WHILE 4
#define DO 5
#define FOR 6
#define INT 7
#define FLOAT 8
#define CHAR 9

//tipos de tokens
#define IDENTIFICADOR 10
#define NUMEROINTEIRO 11
#define NUMEROFLOAT 12
#define DIVISAO 13
#define DIFERENTE 14
#define MENORQUE 15
#define MENOROUIGUAL 16
#define MAIORQUE 17
#define MAIOROUIGUAL 18
#define ATRIBUICAO 19
#define OPERADORIGUAL 20
#define ABREPARENTESES 21
#define FECHAPARENTESES 22
#define CONSTANTECHAR 23
#define ABRECHAVES 24
#define FECHACHAVES 25
#define VIRGULA 26
#define PONTOEVIRGULA 27
#define OPERADORMAIS 28
#define OPERADORMENOS 29
#define OPERADORVEZES 30

#define FIMDEARQUIVO -1

//tipos de erro

#define ERROMULTILINHA -2
#define ERRODIFERENTE -3
#define ERROCHARMALFORMADO -4
#define ERROCARACTERINVALIDO -5
#define ERROFLOATMALFORMADO -6

//fim de definicoes

typedef struct token
{
	int tipo;
	char lexema[30];
	int escopo;
	int cont;
} token;

//estrutura da pilha

typedef struct pilha
{
	token tk;
	struct pilha *prox;

	//struct pilha *ant;

} pilha;

//estrutura da pilha

//cabeçalho usado no parser
token geToken();
token expr_arit();
token fator();           //10
token termo();           //9
token expr_arit_();      //8
token expr_relacional(); //7
token interacao();       //5
token bloco();           //2
token atribuicao();      //6
token comando();         //3
token comando_basico();  //4
token decl_var();        //0
token parser();          //1
//fim cabeçalho

//parte do cabeçalho para semantico

//parte do cabeçalho para semantico

//variaveis globais

int ln=0,tn;//lable globla
int linha, coluna; //linha e coluna para retornar erro
char digitos[10] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
char palavraReservada[9][10] = { "main", "if", "else", "while", "do", "for", "int", "float", "char" };
char *nome_arquivo; //guarda o local do arquivo
FILE *arquivo;      // ponteiro para o arquivo
pilha *tabelaDeSimbolos = NULL;
int escopo_atual = 0;
token t;
//fim de variaveis GLOBAIS

//.. tabela de simbolos

//funcoes da pilha

int isemptyPilha()
{
	if (tabelaDeSimbolos == NULL)
	{
		return 1;
	}
	return 0;
}
void empilha(token t)
{
	pilha *stack = (pilha *)malloc(sizeof(pilha));
	stack->tk = t;
	stack->prox = NULL;

	if (isemptyPilha())
	{
		tabelaDeSimbolos = stack;
		tabelaDeSimbolos->prox = NULL;
	}
	else
	{
		stack->prox = tabelaDeSimbolos;
		tabelaDeSimbolos = stack;
	}
}
token desempilha()
{
	pilha *aux;
	token t;

	if (isemptyPilha())
		printf("pilha ja esta vazia\n");

	else
	{
		t = tabelaDeSimbolos->tk;

		if (tabelaDeSimbolos->prox == NULL)
		{
			free(tabelaDeSimbolos);

			tabelaDeSimbolos = NULL;
		}
		else
		{
			aux = tabelaDeSimbolos;
			tabelaDeSimbolos = tabelaDeSimbolos->prox;
			free(aux);
		}
		return t;
	}
}
void destroyer()
{
	while (!(isemptyPilha()))
	{
		desempilha();
	}
}

int verificaNoEscopo(token t)
{
	pilha *aux;
	token tk_temp;

	aux = tabelaDeSimbolos;
	if (aux == NULL)
		return 0;
	else
	{

		do
		{
			tk_temp = aux->tk;

			if (tk_temp.escopo == t.escopo)
			{
				if (strcmp(tk_temp.lexema, t.lexema) == 0)
				{
					return 1;
				}
			}
			else
			{
				return 0;
			}
			aux = aux->prox;
		} while (aux != NULL);

		return 0;
	}
}
int verificarSeFoiDeclarado(token t)
{
	pilha *aux;
	token tk_temp;

	aux = tabelaDeSimbolos;
	if (aux == NULL)
		return 0;
	else
	{

		do
		{
			tk_temp = aux->tk;

			if (strcmp(tk_temp.lexema, t.lexema) == 0)
			{
				return 1;
			}

			aux = aux->prox;
		} while (aux != NULL);

		return 0;
	}
}
token tipoDoToken(token t)
{
	pilha *aux;
	token tk_temp;

	aux = tabelaDeSimbolos;
	if (aux == NULL)
		return tk_temp;
	else
	{
		do
		{
			tk_temp = aux->tk;

			if (strcmp(tk_temp.lexema, t.lexema) == 0)
			{
				strcpy(tk_temp.lexema,t.lexema);
				tk_temp.tipo=t.tipo;
				tk_temp.escopo=t.escopo;
				tk_temp.cont=t.cont;
				return tk_temp;
			}

			aux = aux->prox;
		} while (aux != NULL);

		return tk_temp;
	}
}
token get_t(token t)
{
	pilha *aux;
	token tk_temp;


	aux = tabelaDeSimbolos;
	if (aux == NULL)
		printf("ERRO elemento nao econtrado\n");
	else
	{
		do
		{
			tk_temp = aux->tk;

			if (strcmp(tk_temp.lexema, t.lexema) == 0)
			{
				return tk_temp;
			}

			aux = aux->prox;
		} while (aux != NULL);

		tk_temp.tipo = '0';
		printf("ERRO elemento nao econtrado\n");
		return tk_temp;

	}
	printf("ERRO elemento nao econtrado\n");

}
void removeScopoAtual()
{
	token tk_temp;
	do
	{
		if (tabelaDeSimbolos == NULL)
			break;
		else
		{
			tk_temp = tabelaDeSimbolos->tk;
			if (tk_temp.escopo == escopo_atual)
			{
				desempilha();
			}
			else
			{
				break;
			}
		}
	} while (tabelaDeSimbolos != NULL);
}
//funcoes da pilha


/*funcao do gerador de codigo*/

token gCI(token t1 , char * op , token t2)
{
	printf("T%i = ",tn);
	printf("%s",t1.lexema);
	if(t1.cont>=0)
	{
		printf("%d",t1.cont);
	}
	printf("%s",op);
	if(op[0] == '!')
	{
		printf("=");
	}
	printf("%s",t2.lexema);
	if(t2.cont>=0)
	{
		printf("%d",t2.cont);
	}
	printf("\n");
	strcpy(t1.lexema,"T");
	t1.cont = tn;
	tn++;
	return t1;
}
/*funcao do gerador de codigo*/

//..

//funçoes do scanner

int is_blank(char c) //branco
{
	if (c == ' ')
		return 1;
	else if (c == '\t')
	{
		coluna = coluna + 4;
		return 1;
	}
	else if (c == '\n')
	{
		coluna = 0;
		linha++;
		return 1;
	}
	return 0;
} //branco

int isdigito(char c) //verifica se é digito
{
	int i;
	for (i = 0; i < 10; i++)
	{
		if (c == digitos[i])
			return 1;
	}
	return 0;
} //verifica se é digito

int lookup(char *buffer) //palavras reservadas
{
	int i;
	for (i = 0; i < 9; i++)
	{
		if (strcmp(buffer, palavraReservada[i]) == 0)
			return (i + 1);
	}
	return 0;
} // verifica palavras reservadas
token getToken() //funcao gettoken
{
	int i, teste; //contador para o buffer; teste para o lookup
	static char c = ' ';
	char buffer[30];
	token t;

inicio: //label do goto "/"

	while (is_blank(c)) //blank
	{
		c = fgetc((arquivo));
		coluna++;
	} //blank

	if (isalpha(c) || c == '_') //identificador
	{
		i = 0;
		buffer[i] = c;
		c = fgetc((arquivo));
		coluna++;
		i++;
		while (isalpha(c) || isdigito(c) || c == '_')
		{
			buffer[i] = c;
			c = fgetc((arquivo));
			coluna++;
			i++;
		}
		buffer[i] = '\0'; //faltou isso no scanner :(
		teste = lookup(buffer);
		if (teste != 0) //verifica se é palavra reservada
		{
			t.tipo = teste;
			return t;
		}
		t.tipo = IDENTIFICADOR;
		strcpy(t.lexema, buffer);
		return t;
	} //fim id

	if (c == '/') //tratamento da barra
	{
		c = fgetc((arquivo));
		coluna++;

		if (c == '/') //comentario de linha
		{
			while (c != '\n')
			{
				if (c == FIMDEARQUIVO) //caso for fim de arquivo
				{
					t.tipo = FIMDEARQUIVO;
					return t;
				}
				c = fgetc((arquivo));
				coluna++;
			}
			linha++;
			coluna = 0;
			c = fgetc((arquivo));
			coluna++;
			goto inicio;
		} //fim comentario de linha

		else if (c == '*') //comentario varias linhas
		{
			c = fgetc((arquivo));
			coluna++;

			do
			{

				if (c == '\n') //conta mais uma linha
				{
					c = fgetc((arquivo));
					coluna = 0;
					linha++;
				}
				else if (c == '*') //comentario mult linhas fechado
				{
					c = fgetc((arquivo));
					coluna++;
					if (c == '/')
					{ //fim comentario multi linha
						c = fgetc((arquivo));
						coluna++;
						goto inicio;
					}
				}
				else
					c = fgetc((arquivo));
				coluna++;
			} while (c != -1);

			//fim de arquivo
			//nao fechou comentario multi linha

			t.tipo = ERROMULTILINHA;

			printf("Erro nao fechou comentario multi linhas\nlinha %d coluna %d\n", linha, coluna);

		} //fim tratamento comentario varias linhas

		else //token de divisao simples
		{
			t.tipo = DIVISAO;
			t.lexema[0] = '/';
			//c = fgetc((arquivo));
			coluna++;
		}
		return t;
	} //fim tratamento de  barra

	if (c == '.') //caso float começando com .
	{
		i = 0;
		buffer[i] = c;

casoFloat: //lable do goto para caso float
		c = fgetc((arquivo));
		coluna++;
		i++; //contador do buffer e de linha andam
		if (isdigito(c))
		{

			buffer[i] = c;
			c = fgetc((arquivo));
			coluna++;
			i++;
			while (isdigito(c))
			{
				buffer[i] = c;
				c = fgetc((arquivo));
				coluna++;
				i++;
			}
			t.tipo = NUMEROFLOAT;
			strcpy(t.lexema, buffer);
			return t;
		}
		else
		{
			t.tipo = ERROFLOATMALFORMADO;
			printf("Erro float mal formado .(faltou o digito) linha %d coluna %d", linha, coluna);
			return t;
		}

	} //Caso float começando com .

	if (isdigito(c)) //caso do int e float
	{
		i = 0;
		while (isdigito(c))
		{
			buffer[i] = c;
			c = fgetc((arquivo));
			coluna++;
			i++;
		}
		if (c == '.')
			goto casoFloat;
		t.tipo = NUMEROINTEIRO;
		buffer[i] = '\0';
		strcpy(t.lexema, buffer);
		return t;
	} //caso int float

	else if (c == '!') //caso do diferente
	{
		c = fgetc((arquivo));
		coluna++;
		if (c == '=')
		{
			t.tipo = DIFERENTE;
		}
		else
		{
			t.tipo = ERRODIFERENTE;
			printf("Erro mal formado operador diferente '!=' linha %d coluna %d\n", linha, coluna);
		}
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //fim diferente

	else if (c == '(') //caso abre parenteses
	{

		t.tipo = ABREPARENTESES;
		c = fgetc((arquivo));
		coluna++;
		return t;

	} //fim do abre parenteses

	else if (c == ')') //fecha parenteses
	{
		t.tipo = FECHAPARENTESES;
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //fim do fecha parenteses

	else if (c == '<') //menor que
	{
		c = fgetc((arquivo));
		coluna++;
		if (c == '=')
		{
			t.lexema[0] ='<';
			t.lexema[1] ='=';
			t.tipo = MENOROUIGUAL;
			c = fgetc((arquivo));
			coluna++;
		}
		else
		{
			t.lexema[0] ='<';
			t.tipo = MENORQUE;
		}
		return t;
	} //menor que fim

	else if (c == '>') //mairo que
	{
		c = fgetc((arquivo));
		coluna++;
		if (c == '=')
		{
			t.lexema[0] ='>';
			t.lexema[1] ='=';
			t.tipo = MAIOROUIGUAL;
			c = fgetc((arquivo));
			coluna++;
		}
		else
		{
			t.lexema[0] ='>';
			t.tipo = MAIORQUE;
		}
		return t;
	} //maior que fim

	else if (c == '=') //igual ou atribuição
	{
		c = fgetc((arquivo));
		coluna++;
		if (c == '=')
		{
			t.tipo = OPERADORIGUAL;
			c = fgetc((arquivo));
			coluna++;
		}
		else
		{
			t.tipo = ATRIBUICAO;
		}
		return t;
	} //fim do igual ou atribuiçao

	else if (c == 39) //constante char ErroParaConcertar
	{
		i = 0;
		buffer[i] = c;
		c = fgetc((arquivo));
		coluna++; i++;
		if (isalpha(c) || isdigito(c))
		{
			buffer[i] = c;
			c = fgetc((arquivo));
			coluna++;
			i++;
			if (c == 39)
			{
				//c = fgetc((arquivo));
				buffer[i] = c;
				coluna++;
				i++;
				buffer[i] = '\0';
				t.tipo = CONSTANTECHAR;
				strcpy(t.lexema, buffer);
				c = fgetc((arquivo));
				return t;
			}
			else
			{
				t.tipo = ERROCHARMALFORMADO;
				printf("Erro constante tipo char mal formada linha %d coluna %d\n", linha, coluna);
				return t;
			}
		}
		else
		{
			t.tipo = ERROCHARMALFORMADO;
			printf("Erro constante tipo char mal formada linha %d coluna %d\n", linha, coluna);
			return t;
		}
	} //constante char

	else if (c == '{') //abre chaves
	{
		t.tipo = ABRECHAVES;
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //fim abre chaves

	else if (c == '}') //fecha chaves
	{
		t.tipo = FECHACHAVES;
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //fecha chaves

	else if (c == ',') // ,
	{
		t.tipo = VIRGULA;
		c = fgetc((arquivo));
		coluna++;
		return t;
	} // caso virgula

	else if (c == ';') // ;
	{
		t.tipo = PONTOEVIRGULA;
		c = fgetc((arquivo));
		coluna++;
		return t;
	} // caso ponto e virgula

	else if (c == '+') // op +
	{
		t.tipo = OPERADORMAIS;
		t.lexema[0] = '+';
		c = fgetc((arquivo));
		coluna++;
		return t;
	} // operador soma

	else if (c == '-') // op -
	{
		t.tipo = OPERADORMENOS;
		t.lexema[0]='-';
		t.lexema[1]='\0';
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //operador subtraçao

	else if (c == '*')
	{ //op *
		t.tipo = OPERADORVEZES;
		t.lexema[0]='*';
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //operador multiplicaçao

	else if (c == FIMDEARQUIVO) //Caso do fim de arquivo
	{
		t.tipo = FIMDEARQUIVO;
		return t;
	} //fim de arquivo

	else //Caracter invalido
	{
		t.tipo = ERROCARACTERINVALIDO;
		printf("Erro caracter invalido linha %d coluna %d\n", linha, coluna);
		c = fgetc((arquivo));
		coluna++;
		return t;
	} //caracter invalido

} //fim da funcao gettoken

//fim do scanner

//Inicio do parser

token decl_var() //finalizado 0
{
	token temporario;
	if (t.tipo == INT || t.tipo == FLOAT || t.tipo == CHAR) //Verifica se é tipo
	{
		temporario.tipo = t.tipo; //tipo da variavel para adicionar na pilha

		t = getToken();
		if (t.tipo == IDENTIFICADOR)
		{
			strcpy(temporario.lexema, t.lexema); //lexema variavel temporaria
			temporario.escopo = escopo_atual;

			if (verificaNoEscopo(temporario)) //verifica se existe msm variavel no msm escopo
				printf("ja existe essa variavel linha %d coluna %d", linha, coluna);
			else
				empilha(temporario);

			t = getToken();
			while (t.tipo == VIRGULA)
			{
				t = getToken();
				if (t.tipo == IDENTIFICADOR)
				{
					strcpy(temporario.lexema, t.lexema); //lexema variavel temporaria
					temporario.escopo = escopo_atual;

					if (verificaNoEscopo(temporario)) //verifica se existe msm variavel no msm escopo
						printf("ja existe essa variavel linha %d coluna %d", linha, coluna);
					else
						empilha(temporario);

					t = getToken();
				}
				else
				{
					printf("Erro esperado um identificador linha %d coluna %d", linha, coluna);
					return t;
				}
			}

			if (t.tipo == PONTOEVIRGULA)
				t = getToken();
			else
			{
				printf("Esperado ponto e virgula linha %d coluna %d", linha, coluna);
				return t;
			}
		}
		else
		{
			printf("Esperado um identificador linha %d coluna %d", linha, coluna);
			return t;
		}
	}

	else
	{
		printf("esperado algum tipo de dado linha %d coluna %d", linha, coluna);
		return t;
	}
	return t;
}

token fator() //finalizado 10
{

	token t1;
	t1.cont= -1;

	if (t.tipo == ABREPARENTESES)
	{
		t = getToken();
		if (t.tipo == ABREPARENTESES || t.tipo == IDENTIFICADOR || t.tipo == NUMEROFLOAT || t.tipo == NUMEROINTEIRO || CONSTANTECHAR) //fi(expr_art)
		{
			t1 = expr_arit();
			if (t.tipo == FECHAPARENTESES)
			{
				t = getToken();
				return t1;
			}
			//t=getToken(t);
		}
	}
	else if (t.tipo == IDENTIFICADOR)
	{
		if (!(verificarSeFoiDeclarado(t)))
			printf("Erro variavel nao declarada linha %d coluna %d", linha, coluna);
		t1 = get_t(t);
		t = getToken();
		return t1;
	}
	else if (t.tipo == NUMEROFLOAT || t.tipo == NUMEROINTEIRO || t.tipo == CONSTANTECHAR)
	{
		strcpy(t1.lexema, t.lexema);
		t1.tipo = t.tipo;
		t = getToken();
	}
	else
	{
		printf("Esperado first de fator linha %d coluna %d", linha, coluna);
	}

	return t1;
}
token termo() //falta fazer  9
{
	token t1, t2;
	int flag = 1;
	char op[3];

	if (t.tipo == ABREPARENTESES || t.tipo == IDENTIFICADOR || t.tipo == NUMEROFLOAT || t.tipo == NUMEROINTEIRO || t.tipo == CONSTANTECHAR) //fi(fator)
	{
		t1 = fator();
	}
	else
	{
		printf("Erro esperado um Fator %d linha %d coluna", linha, coluna);
	}
	while (t.tipo == DIVISAO || t.tipo == OPERADORVEZES)
	{
		if (t.tipo == DIVISAO)
			flag = 0;
		strcpy(op,t.lexema);
		t = getToken();
		t2 = fator();
		//comparacao
		if (t1.tipo == CHAR || t1.tipo == INT || t1.tipo == FLOAT || t1.tipo == CONSTANTECHAR || t1.tipo == NUMEROINTEIRO || t1.tipo == NUMEROFLOAT)
		{
			if ((t1.tipo == CHAR || t1.tipo == CONSTANTECHAR) && (t2.tipo != CHAR && t2.tipo != CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis char so opera com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis int nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis Float nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == INT && t2.tipo == NUMEROINTEIRO))
			{
				if (t1.tipo == FLOAT)
				{
					t1.tipo = INT;
				}
				else
				{
					t1.tipo = NUMEROINTEIRO;
				}
				printf ("T%i = (float) %s\n", tn, t2.lexema);
				strcpy (t2.lexema, "T");
				t2.cont = tn;
				tn++;

			}
			else if (flag == 0 && (t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == INT || t2.tipo == NUMEROINTEIRO))
			{
				if (t1.tipo == INT)
				{
					t1.tipo = FLOAT;
				}
				else
				{
					t1.tipo = NUMEROFLOAT;
				}
				flag = 1;
				printf ("T%i = (float) %s\n", tn, t1.lexema);
				strcpy (t1.lexema, "T");
				t1.cont = tn;
				tn++;

			}
		}
		t1 = gCI(t1, op, t2);
		t1 = t2;
	}
	return t1;
}

token expr_arit_() //finalizado 8.2
{
	char op[3];
	token t1, t2;
	t1.tipo = 0;
	t2.tipo = 0;
	if (t.tipo == OPERADORMAIS || t.tipo == OPERADORMENOS)
	{
		strcpy(op,t.lexema);
		t = getToken();
		t1 = termo();
		t2 = expr_arit_();
		if (t1.tipo == CHAR || t1.tipo == INT || t1.tipo == FLOAT || t1.tipo == CONSTANTECHAR || t1.tipo == NUMEROINTEIRO || t1.tipo == NUMEROFLOAT)
		{
			if ((t1.tipo == CHAR || t1.tipo == CONSTANTECHAR) && (t2.tipo != CHAR && t2.tipo != CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis char so opera com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis int nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis Float nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == INT || t2.tipo == NUMEROINTEIRO))
			{
				if (t2.tipo == INT)
				{
					t2.tipo = FLOAT;
				}
				else
				{
					t2.tipo = NUMEROFLOAT;
				}
				printf("T%d = (float )%s\n",tn,t1.lexema);
				strcpy(t1.lexema,"T");
				t1.cont = tn;
				tn++;
			}
			else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == FLOAT || t2.tipo == NUMEROFLOAT))
			{
				if (t1.tipo == INT)
				{
					t1.tipo = FLOAT;
				}
				else
				{
					t1.tipo = NUMEROINTEIRO;
				}
				printf("T%d = (float) %s\n",tn,t2.lexema);
				strcpy(t2.lexema,"T");
				t2.cont = tn;
				tn++;
			}
			t1 = gCI(t1,op,t2);
		}
	}
	return t1;
}
token expr_arit() //finalizado 8
{
	char op[3];
	token t1, t2;
	if (t.tipo == ABREPARENTESES || t.tipo == IDENTIFICADOR || t.tipo == NUMEROINTEIRO || t.tipo == CONSTANTECHAR || t.tipo == NUMEROFLOAT) //fi(termo)
	{
		t1 = termo();
		strcpy(op,t.lexema);

		t2 = expr_arit_();
		if (t2.tipo == 0)
			return t1;
		if (t1.tipo == CHAR || t1.tipo == INT || t1.tipo == FLOAT || t1.tipo == CONSTANTECHAR || t1.tipo == NUMEROINTEIRO || t1.tipo == NUMEROFLOAT)
		{
			if ((t1.tipo == CHAR || t1.tipo == CONSTANTECHAR) && (t2.tipo != CHAR && t2.tipo != CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis char so opera com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis int nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
			{
				printf("Erro tipos incompativeis Float nao compara com char linha %d coluna %d", linha, coluna);
			}
			else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == INT || t2.tipo == NUMEROINTEIRO))
			{
				if (t2.tipo == INT)
				{
					t2.tipo = FLOAT;
				}
				else
				{
					t2.tipo = NUMEROFLOAT;
				}
				printf("T%d = (float )%s\n",tn,t1.lexema);
				strcpy(t1.lexema,"T");
				t1.cont = tn;
				tn++;
			}
			else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == FLOAT || t2.tipo == NUMEROFLOAT))
			{
				if (t1.tipo == INT)
				{
					t1.tipo = FLOAT;
				}
				else
				{
					t1.tipo = NUMEROINTEIRO;
				}
				printf("T%d = (float) %s\n",tn,t2.lexema);
				strcpy(t2.lexema,"T");
				t2.cont = tn;
				tn++;
			}
			t1 = gCI(t1,op,t2);
		}
		else
		{
			printf("erro esperado um termo linha %d coluna %d", linha, coluna);
		}
	}
	return t1;
}

/*
exp-> termo exp_
exp_-> + exp_
exp_-> - exp_
exp_-> termo
*/

token expr_relacional() //finalizado 7
{
	token t1, t2;
	char tipo[3];

	if (t.tipo == ABREPARENTESES || t.tipo == IDENTIFICADOR || t.tipo == NUMEROINTEIRO || t.tipo == CONSTANTECHAR || t.tipo == NUMEROFLOAT) //fi(expr_arit)==fi(termo)
	{
		t1 = expr_arit();

		strcpy(tipo,t.lexema);//gci

		if (t.tipo == OPERADORIGUAL || t.tipo == MAIORQUE || t.tipo == MENORQUE || t.tipo == MENOROUIGUAL || t.tipo == MAIOROUIGUAL || t.tipo == DIFERENTE) //operador relacional
		{
			t = getToken();
			if (t.tipo == ABREPARENTESES || t.tipo == IDENTIFICADOR || t.tipo == NUMEROINTEIRO || t.tipo == CONSTANTECHAR || t.tipo == NUMEROFLOAT) //fi(expr_arit)==fi(termo)
			{
				t2 = expr_arit();
				if ((t1.tipo == CHAR || t1.tipo == CONSTANTECHAR) && (t2.tipo != CHAR && t2.tipo != CONSTANTECHAR))
				{
					printf("Erro tipos incompativeis char so opera com char linha %d coluna %d", linha, coluna);
				}
				else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
				{
					printf("Erro tipos incompativeis int nao compara com char linha %d coluna %d", linha, coluna);
				}
				else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == CHAR && t2.tipo == CONSTANTECHAR))
				{
					printf("Erro tipos incompativeis Float nao compara com char linha %d coluna %d", linha, coluna);
				}
				else if ((t1.tipo == FLOAT || t1.tipo == NUMEROFLOAT) && (t2.tipo == INT && t2.tipo == NUMEROINTEIRO))
				{
					if (t2.tipo == INT)
						t2.tipo = FLOAT;
					else
						t2.tipo = NUMEROFLOAT;
					printf("T%d = (float) %s",tn,t2.lexema);
					if(t2.cont>=0)
					{
						printf("%d",t2.cont);
					}
					printf("\n");
					strcpy(t2.lexema,"T");
					t2.cont = tn;
					tn++;

				}
				else if ((t1.tipo == INT || t1.tipo == NUMEROINTEIRO) && (t2.tipo == FLOAT && t2.tipo == NUMEROFLOAT))
				{
					if (t1.tipo == INT)
						t1.tipo = FLOAT;
					else
						t1.tipo = NUMEROFLOAT;
					//gci
					printf("T%d = (float) %s",tn,t1.lexema);
					if(t1.cont>=0)
					{
						printf("%d",t1.cont);
					}
					printf("\n");
					strcpy(t1.lexema,"T");
					t1.cont = tn;
					tn++;
				}
				t1 = gCI(t1,tipo,t2);

				return t1;
			}
		}
		else
		{
			printf("Erro esperado um operador relacional linha %d coluna %d", linha, coluna);
		}
	}
	else
	{
		printf("Erro esperado expressao aritimetrica linha %d coluna %d", linha, coluna);
	}
	return t1;
}

token atribuicao() //Finalizado 6
{
	int tipo;
	token recebido;
	if (t.tipo == IDENTIFICADOR)
	{
		t = getToken();
		if (t.tipo == ATRIBUICAO)
		{
			t = getToken();
			recebido = expr_arit();

			if (t.tipo == PONTOEVIRGULA)
			{
				t = getToken();
				return recebido;
			}
			else
			{
				printf("Erro esperado token ; linha %d coluna %d", linha, coluna);
			}
		}
		else
		{
			printf("Erro esperado token igual linha %d coluna %d", linha, coluna);
		}
	}
	else
	{
		printf("Erro esperado um identificador linha %d coluna %d", linha, coluna);
	}
	return t;
}

token interacao() //finalizado 5
{
	int lableLocal;
	token aux;

	printf("L%d\n",ln);
	if (t.tipo == WHILE)
	{
		t = getToken();
		if (t.tipo == ABREPARENTESES)
		{
			t = getToken();
			aux = expr_relacional();
			if (t.tipo == FECHAPARENTESES)
			{
				t = getToken();
				//gerador if
				printf("if");
				printf("%s",aux.lexema);
				if(aux.cont >= 0)
				{
					printf("%d",aux.lexema);
				}
				lableLocal = ln;
				ln++;
				printf(" == 0 goto L%d\n",ln);

				t = comando();

				printf("goto L%d\n",lableLocal);
				printf("L%d:\n",ln);
				ln++;
				return t;
			}
			else
			{
				printf("Erro esperado token fecha parenteses linha %d coluna %d", linha, coluna);
			}
		}
		else
		{
			printf("Erro esperado token abre parenteses linha %d coluna %d", linha, coluna);
		}
	}
	else if (t.tipo == DO)
	{
		t = getToken();
		t = comando();
		if (t.tipo == WHILE)
		{
			t = getToken();
			if (t.tipo == ABREPARENTESES)
			{
				t = getToken();
				aux = expr_relacional(); // modificado
				if (t.tipo == FECHAPARENTESES)
				{
					t = getToken();
					if (t.tipo == PONTOEVIRGULA)
					{
						t = getToken();
						printf("if");
						printf("%s",aux.lexema);
						if(aux.cont>=0)
						{
							printf("%d",aux.cont);
						}
						printf("!=0 goto L%d\n",ln);
						ln++;
					}
					else
					{
						printf("Erro esperado token abre parenteses linha %d coluna %d", linha, coluna);
					}
				}
				else
				{
					printf("Erro esperado token fecha parenteses linha %d coluna %d", linha, coluna);
				}
			}
			else
			{
				printf("Erro esperado token abre parenteses linha %d coluna %d", linha, coluna);
			}
		}
		else
		{
			printf("Erro esperado token while linha %d coluna %d", linha, coluna);
		}
	}
	else
	{
		printf("Erro esperado token do ou while linha %d coluna %d", linha, coluna);
	}
	return t;
}

token comando_basico() //finalizado 4
{
	token token1, tokenDeRetorno;

	if (t.tipo == IDENTIFICADOR)
	{
		if (!(verificarSeFoiDeclarado(t)))
		{
			printf("Erro variavel nao foi declarada linha %d coluna %d\n", linha, coluna);
			exit(0);
			return t;
		}
		token1 = tipoDoToken(t);
		tokenDeRetorno = atribuicao();

		if (token1.tipo == CHAR)
		{
			if (tokenDeRetorno.tipo != CHAR && tokenDeRetorno.tipo != CONSTANTECHAR)
				printf("Erro esperado um token char linha %d coluna %d", linha, coluna);
		}
		else if (token1.tipo == INT)
		{
			if (tokenDeRetorno.tipo != INT && tokenDeRetorno.tipo != NUMEROINTEIRO)
				printf("Erro esperado um token inteiro linha %d coluna %d", linha, coluna);
		}
		else if (token1.tipo == FLOAT)
		{
			if (tokenDeRetorno.tipo != INT && tokenDeRetorno.tipo != NUMEROINTEIRO &&
				tokenDeRetorno.tipo != FLOAT && tokenDeRetorno.tipo != NUMEROFLOAT)
				printf("Erro esperado um token FLOAT linha %d coluna %d", linha, coluna);
			else if (tokenDeRetorno.tipo == INT && tokenDeRetorno.tipo == NUMEROINTEIRO)
			{
				if (tokenDeRetorno.tipo == INT)
					tokenDeRetorno.tipo = FLOAT;
				else
					tokenDeRetorno.tipo = NUMEROFLOAT;
				printf("T%d = (float)%s",tn,tokenDeRetorno.lexema);
				if(tokenDeRetorno.cont>=0)
				{
					printf("%d",tokenDeRetorno.cont);
				}
				printf("\n");
				strcpy(tokenDeRetorno.lexema,"T");
				tokenDeRetorno.cont = tn;
				tn++;
			}
			printf("%s = %s",token1.tipo,tokenDeRetorno.lexema);
			if(tokenDeRetorno.cont>=0)
			{
				printf("%d",tokenDeRetorno.cont);
			}
			printf("\n");
		}
	}
	else
		t = bloco();

	return t;
}

token comando() //finalizado 3
{
	int lable,lable1;
	token op;
	int contador = 0;

	if (t.tipo == IDENTIFICADOR || t.tipo == ABRECHAVES) //fi(comando basico)
	{
		t = comando_basico();
	}
	else if (t.tipo == WHILE || t.tipo == DO) //fi(fi interação)
	{
		t = interacao();
	}
	else if (t.tipo == INT || t.tipo == FLOAT || t.tipo == CHAR || t.tipo == NUMEROINTEIRO || t.tipo == NUMEROFLOAT || t.tipo == CONSTANTECHAR)
	{
		printf("Erro nao pode se declarar variavel %s fora de bloco\n linha %d coluna %d", t.lexema, linha, coluna);
		destroyer(); exit(0);
	}
	else //tem q ser if
	{
		if (t.tipo == IF)
		{
			t = getToken();
			if (t.tipo == ABREPARENTESES)
			{
				t = getToken();
				op = expr_relacional();

				//t = getToken();
				if (t.tipo == FECHAPARENTESES)
				{
					t = getToken();

					//gerador
					printf("if ");

					printf("%s", op.lexema);
					if (op.cont >= 0)
					{
						printf("%d", op.cont);
					}
					//gerador
					printf(" == 0 goto L%i\n", ln);

					lable1=ln;
					ln++;
				}
				else
				{
					printf("Erro esperado token fecha parenteses linha %d coluna %d", linha, coluna);
				}
			}
			else
			{
				printf("Erro token esperado abre parenteses linha %d coluna %d", linha, coluna);
				return t;
			}
			if (t.tipo == IF || t.tipo == IDENTIFICADOR || t.tipo == ABRECHAVES || t.tipo == WHILE || t.tipo == DO) //first de comando
			{
				t = comando();
				if (t.tipo == ELSE)
				{
					lable = ln;
					printf("goto L%d\n", lable);
					printf("L%d:\n", ln - 1);
					t = getToken();
					t = comando();
					printf("L%d:\n", lable);
				}
				else// if(t.tipo!=ELSE)
				{
					printf("L%d:\n", lable1);
				}
			}		
			else
			{
				printf("Erro esperado token inicial de comando linha %d coluna %d", linha, coluna);
				return t;
			}
		}
		else
		{ //erro
			printf("Erro esperado Token if linha %d coluna %d", linha, coluna);
			return t;
		}
	}
	return t;
}

token bloco() //Finalizado 2
{
	if (t.tipo == ABRECHAVES)
	{
		t = getToken();

		escopo_atual++;

		while (t.tipo == INT || t.tipo == FLOAT || t.tipo == CHAR) //fi(decl_var)
		{
			t = decl_var();
			//declaração de variavel
		}
		while (t.tipo == ABRECHAVES || t.tipo == IDENTIFICADOR || t.tipo == IF || t.tipo == WHILE || t.tipo == DO) //fi(comando)
		{
			t = comando();
			//COMANDO
		}
		if (t.tipo == FECHACHAVES) //aceito
		{
			t = getToken();

			removeScopoAtual();
			escopo_atual--;

			return t;
		}
		else
		{
			printf("Erro no bloco\nFaltou fecha chaves linha %d coluna %d", linha, coluna);
			return t;
		}
	}
	else
	{
		printf("Erro no bloco\nFaltou abre chaves linha %d coluna %d", linha, coluna);
		return t;
	}
}

token parser() //função main 1
{
	if (t.tipo == INT)
	{
		t = getToken();
		if (t.tipo == MAIN)
		{
			t = getToken();
			if (t.tipo == ABREPARENTESES)
			{
				t = getToken();
				if (t.tipo == FECHAPARENTESES)
				{
					t = getToken();
					t = bloco();
					if (t.tipo != -1)
					{
						printf("\nErro ultimo tk nao eh EOF linha %d coluna %d\n", linha, coluna);
					}
					return t;
				}
				else
				{
					printf("faltou fecha parenteses linha %d coluna %d", linha, coluna);
					return t;
				}
			}
			else
			{
				printf("erro faltou abre parenteses linha %d coluna %d", linha, coluna);
				return t;
			}
		}
		else
		{
			printf("erro faltou main linha %d coluna %d", linha, coluna);
			return t;
		}
	}
	else
	{
		printf("erro faltou int linha %d coluna %d", linha, coluna);
		return t;
	}
}

//fim do parser
void abre_arquivo()
{

	if (nome_arquivo == NULL)
	{
		printf("Erro!\n");
		printf("Deve passar nome do arquivo como parametro\n");
		exit(1);
	}

	arquivo = fopen(nome_arquivo, "rt");

	if ((arquivo) == NULL)
	{
		printf("Erro ao abrir arquivo\n");
		exit(1);
	}
	else
		printf("Arquivo aberto com sucesso\n");
}

void fecha_arquivo()
{
	int status;

	status = fclose(arquivo);

	if (status == 0)
		printf("\nArquivo fechado com sucesso\n");
	else
		printf("\nErro ao fechar arquivo");
}

int main(int argc, char *argv[])
{
	escopo_atual = 0;
	nome_arquivo = (argv[1]); //segundo argumento é o nome do arquivo de texto

	abre_arquivo(); //Abre arquivo

	/*t = getToken();
	empilha(t);

	t = getToken();
	empilha(t);

	destroyer();
	*/

	t = getToken();

	t = parser(); //parser

	printf("\nUltimo token %d\n", t.tipo);

	fecha_arquivo(); //Fecha arquivo

	return 0;
}
